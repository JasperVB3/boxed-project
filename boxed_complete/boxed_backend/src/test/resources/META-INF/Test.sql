INSERT INTO dbo.CellBlock (cellBlockId) VALUES ('A');
INSERT INTO dbo.CellBlock (cellBlockId) VALUES ('B');
INSERT INTO dbo.CellBlock (cellBlockId) VALUES ('C');
INSERT INTO dbo.Cell (cellNr, isolationCell, size, cellBlock) VALUES ('A1',0,'1','A');
INSERT INTO dbo.Cell (cellNr, isolationCell, size, cellBlock) VALUES ('A2',1,'1','A');
INSERT INTO dbo.Cell (cellNr, isolationCell, size, cellBlock) VALUES ('A3',1,'1','A');
INSERT INTO dbo.Cell (cellNr, isolationCell, size, cellBlock) VALUES ('B1',0,'1','B');
INSERT INTO dbo.Cell (cellNr, isolationCell, size, cellBlock) VALUES ('B2',1,'1','B');
INSERT INTO dbo.Cell (cellNr, isolationCell, size, cellBlock) VALUES ('B3',1,'1','B');
INSERT INTO dbo.Crime (name, punishment) VALUES ('Moord', 3650);
INSERT INTO dbo.Crime (name, punishment) VALUES ('Diefstal', 200);
INSERT INTO dbo.Crime (name, punishment) VALUES ('Gevecht', 14);
INSERT INTO dbo.Guard (name) VALUES ('Bob');
INSERT INTO dbo.Guard (name) VALUES ('Bob');
INSERT INTO dbo.Guard (name) VALUES ('Bob');
INSERT INTO dbo.Job (name, duration) VALUES ('Wasserette', 365);
INSERT INTO dbo.Job (name, duration) VALUES ('WcKuisen', 14);
INSERT INTO dbo.Job (name, duration) VALUES ('Zagen', 60);
INSERT INTO dbo.Prisoner (name, isolated, releaseDate) VALUES ('Gert', 0, 50);
INSERT INTO dbo.Prisoner (name, isolated, releaseDate) VALUES ('Gert', 0, 100);
INSERT INTO dbo.Prisoner (name, isolated, releaseDate) VALUES ('Gert', 0, 200);
INSERT INTO dbo.Day DEFAULT VALUES;
INSERT INTO dbo.Day DEFAULT VALUES;
INSERT INTO dbo.Day DEFAULT VALUES;
INSERT INTO dbo.Day DEFAULT VALUES;
INSERT INTO dbo.Day DEFAULT VALUES;
